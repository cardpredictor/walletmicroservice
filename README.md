# Card Predictor Microservices
## Set up project
1. Run the `database.sql` on your local database to get everything setup.
2. `npm install`
3. Update migrations `npx sequelize db:migrate` `npx sequelize db:seed:all`
## Run
The project works on the windows system (didn't test on the linux)

`npm start`

## How to use Sequelize CLI
to create model run `sequelize model:create --name <model_name> --attributes`

to migrate run: `sequelize db:migrate`

to undo migrate run: `sequelize db:migrate:undo`

to seed run: `sequelize db:seed:all`

to create migration run: `sequelize migration:create --name <migration_name>`

## Endpoint list
`POST` `/users`, requires body `{username: string, password: string}` returns user and wallet data

`POST` `/users/login`, requires body `{username, password}` returns auth token