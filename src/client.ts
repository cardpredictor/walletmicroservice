import request from 'request';
import HttpStatus from 'http-status-codes';
import {ResponseObject} from "./types/ResponseObject";

let serviceUri: string;

export class WalletClient {
  constructor(host: string, port: number) {
    serviceUri = `http://${ host }:${ port }`;
  }

  call(req: any, res: any, next: any) {
    request(`${ serviceUri }${ req.path }`, {
      json: req.body,
      method: req.method
    }, (err: any, response: any) => {
      if (!err) {
        try {
          const body = typeof response.body !== 'object' ? JSON.parse(response.body) : response.body;
          res.status(body.status);
          res.json(body);
        } catch (error) {
          res.status(HttpStatus.BAD_REQUEST);
          res.json(error || {
            status: HttpStatus.BAD_REQUEST,
            message: 'microservice is down'
          });
        }
      } else {
        res.status(HttpStatus.BAD_REQUEST);
        res.json(err || {
          status: HttpStatus.BAD_REQUEST,
          message: 'microservice is down'
        });
      }
    });
  }

  callPromise(method: string, path:string, body: any, headers: any): Promise<ResponseObject> {
    return new Promise((resolve, reject) => {
      request(`${ serviceUri }${ path }`, {
        method,
        json: body,
        headers
      }, (err: any, res: any) => {
        if (err)
          reject(err);

        const response = typeof res.body !== 'object' ? JSON.parse(res.body) : res.body;

        if (response.status === 200)
          resolve(response);

        reject(response)
      })
    });
  }
}
