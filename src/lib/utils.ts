import crypto from "crypto";
import {HexBase64Latin1Encoding} from "crypto";
import {WalletData} from "../types/WalletData";
import NodeRSA = require("node-rsa");
import {Wallet} from "../types/Wallet";

export function genRandomString(length: number) {
  return crypto.randomBytes(Math.ceil(length/2))
    .toString('hex') /** convert to hexadecimal format */
    .slice(0,length);   /** return required number of characters */
}

export function hashTextWithSecret(text: string, secret: string, algorithm = 'sha256', encoding: HexBase64Latin1Encoding = 'hex') {
  return crypto
    .createHmac(algorithm, secret)
    .update(text, 'utf8')
    .digest(encoding);
}

export function checksumText(str: string, algorithm = 'sha256', encoding: HexBase64Latin1Encoding = 'hex') {
  return crypto
    .createHash(algorithm)
    .update(str, 'utf8')
    .digest(encoding);
}

export function encryptText(text: string, key: any, algorithm = 'aes-256-ctr') {
  const cipher = crypto.createCipher(algorithm, key);
  let crypted = cipher.update(text, 'utf8', 'hex');
  crypted += cipher.final('hex');
  return crypted;
}

export function decryptText(text: string, key: any, algorithm = 'aes-256-ctr') {
  const decipher = crypto.createDecipher(algorithm, key);
  let dec = decipher.update(text, 'hex', 'utf8');
  dec += decipher.final('utf8');
  return dec;
}

export function decryptWallet(walletData: any, privateKey: string, walletKey: string): Promise<WalletData> {
  // decrypt the wallet key
  const decryptedWalletKey = decryptWalletKey(privateKey, walletKey);

  // decrypt wallet data
  return JSON.parse(decryptText(walletData, decryptedWalletKey));
}

function decryptWalletKey(privateKey: string, walletKey: string): Buffer {
  // get private key
  const key = new NodeRSA(privateKey);

  // decrypt the wallet key
  return Buffer.from(key.decrypt(walletKey));
}

export function encryptWallet(walletData: WalletData, privateKey: string, walletKey: string): string {
  // decrypt the wallet key
  const decryptedWalletKey = decryptWalletKey(privateKey, walletKey);

  return encryptText(JSON.stringify(walletData), decryptedWalletKey);
}