import HttpStatus from 'http-status-codes';
import jwt from 'jwt-then';
import config from 'config';

export async function authorized(req: any, res: any, next: any) {
  const token = req.header('authorization') || req.headers.authorization;

  let user = null;
  if (token) {
    try {
      const decoded = await jwt.verify(token, config.get('JWT.secret'));
      // @ts-ignore
      user = await req.context.userRepository.getUserById(decoded.id);
    } catch (error) {
      res.status(HttpStatus.UNAUTHORIZED);
      res.send({
        status: HttpStatus.UNAUTHORIZED,
        message: 'Invalid token'
      });

      return;
    }
  }

  if (!token || !user) {
    res.status(HttpStatus.UNAUTHORIZED);
    res.send({
      status: HttpStatus.UNAUTHORIZED,
      message: 'Invalid token'
    });

    return;
  }

  req.authorizedUser = user;

  // The user is authorised so continue
  return next();
}
