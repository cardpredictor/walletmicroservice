import models from '../../models'
import { User } from '../types/User';

// @ts-ignore / user is declared in models/wallet
const userModel = models.user;

export class UserRepository {

  async create({ username, password, privateKey, publicKey, walletKey }: User) {
    const result: any = await userModel.create({
      // @ts-ignore
      username: username.toLowerCase(),
      password,
      privateKey,
      publicKey,
      walletKey
    });

    if (!result) {
      return null;
    }

    return result.dataValues;
  }

  async getByUsername(username: any) {
    return await userModel.findOne({
      where: { username: username.toLowerCase() }
    });
  }

  async getUserById(id: number) {
    const data = await userModel.findOne({
      where: { id }
    });

    if (!data) {
      return null;
    }

    return data.dataValues;
  }
}
