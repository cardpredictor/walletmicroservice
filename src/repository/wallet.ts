import models from '../../models'
import { Wallet } from '../types/Wallet';

// @ts-ignore
const walletModel = models.wallet;

export class WalletRepository {

  async create({ userId, data }: Wallet) {
    const result: any = await walletModel.create({
      userId,
      data
    });

    if (!result) {
      return null;
    }

    return result.dataValues;
  }

  async getWalletByUserId(userId: string): Promise<Wallet> {
    return await walletModel.findOne({
      where: {
        userId
      }
    })
  }

  async update({id, data }: Wallet) {
    const result = await walletModel.update({ data }, {
      where: {
        id
      }
    });

    if (!result || result[0] === 0) {
      return 0;
    }

    return result[0];
  }
}