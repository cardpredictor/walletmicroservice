import { Context } from '../types/Context';
import HttpStatus from 'http-status-codes';
import { User } from '../types/User';
import { ResponseObject } from '../types/ResponseObject';
import NodeRSA from 'node-rsa';
import { decryptText, encryptText } from '../lib/utils';
import crypto from 'crypto';

export class CreateNewUserService {
  context: Context;

  constructor(context: Context) {
      this.context = context;
  }

  async execute({ username, password }: User): Promise<ResponseObject> {
    try {
      const existingUser = await this.context.userRepository.getByUsername(username);

      if (existingUser) {
        return {
          status: HttpStatus.FORBIDDEN,
          message: `User with username ${ username } already exists`
        }
      }


      // @ts-ignore
      // initialise a key
      const key = new NodeRSA({
        b: 2048,
        e: 65537
      });

      // export private and public keys
      const privateKey = key.exportKey('pkcs8-private-pem');
      const publicKey = key.exportKey('pkcs8-public-pem');

      // initialize wallet key
      const walletKey = crypto.randomBytes(32);

      // encrypt wallet key
      const encryptedWalletKey = key.encrypt(walletKey, 'base64');

      // save the user
      const user: User = await this.context.userRepository.create({
        walletKey: encryptedWalletKey,
        privateKey,
        publicKey,
        username,
        password
      });

      // user start with balance 1000
      const dataWallet = {
        gamesLoss: 0,
        gamesWin: 0,
        balance: 1000
      };

      // encrypt wallet data
      const data = encryptText(JSON.stringify(dataWallet), walletKey);

      // create wallet
      await this.context.walletRepository.create({
        userId: user.id,
        data
      });

      // do not send privateKey, walletKey, passwordSalt and password
      delete user.privateKey;
      delete user.walletKey;
      delete user.password;
      delete user.passwordSalt;

      return {
        status: HttpStatus.OK,
        message: 'OK',
        data: {
          user,
          wallet: JSON.parse(decryptText(data, walletKey))
        }
      }
    } catch (error) {
      return {
        status: HttpStatus.BAD_REQUEST,
        message: error.message || error
      }
    }
  }
}


