import { Context } from '../types/Context';
import HttpStatus from 'http-status-codes';
import jwt from 'jwt-then';
import { User } from '../types/User';
import config from 'config'
import { ResponseObject } from "../types/ResponseObject";

export class LoginUserService {
  context: Context;

  constructor(context: Context) {
    this.context = context;
  }

  async execute({ username, password }: User): Promise<ResponseObject> {
    try {
      // check if username on password in provided
      if (username === undefined) {
        return {
          status: HttpStatus.BAD_REQUEST,
          message: 'Please provide a username'
        }
      }

      if (password === undefined) {
        return {
          status: HttpStatus.BAD_REQUEST,
          message: 'Please provide a password'
        };
      }

      // get user model to verify password
      const userModel = await this.context.userRepository.getByUsername(username);

      if (!userModel) {
        return {
          status: HttpStatus.NOT_FOUND,
          message: 'Invalid Login'
        };
      }

      // check password if it is equal with provided one
      if (!userModel.verifyPassword(password)) {
        return {
          status: HttpStatus.UNAUTHORIZED,
          message: 'Invalid Login'
        };
      }

      // get user information
      const user = userModel.dataValues;

      // sign the token
      const token = await jwt.sign({
        username,
        id: user.id
      }, config.get('JWT.secret'));

      return {
        status: HttpStatus.OK,
        message: 'OK',
        data: {
          token
        }
      };
    } catch (error) {
      return {
        status: HttpStatus.BAD_REQUEST,
        message: error.message || error
      }
    }
  }
}