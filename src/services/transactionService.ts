import { Context } from "../types/Context";
import HttpStatus from "http-status-codes";
import jwt from 'jwt-then';
import config from 'config';
import { ResponseObject } from "../types/ResponseObject";
import  {Wallet } from "../types/Wallet";
import { WalletData } from "../types/WalletData";
import { decryptWallet, encryptWallet } from "../lib/utils";

export class TransactionService {
  context: Context;

  constructor(context: Context) {
    this.context = context;
  }

  async execute(token: string, { amount , isDeposit }: any): Promise<ResponseObject> {
    try {
      const decoded = await jwt.verify(token, config.get('JWT.secret'));
      // @ts-ignore
      const user = await this.context.userRepository.getUserById(decoded.id);

      if (!user) {
        return {
          status: HttpStatus.NOT_FOUND,
          message: 'User not found'
        }
      }

      const wallet: Wallet = await this.context.walletRepository.getWalletByUserId(user.id);

      // decrypt wallet data
      const walletData: WalletData = await decryptWallet(wallet.data, user.privateKey, user.walletKey);

      if (!isDeposit && walletData.balance < amount) {
        // return success response due to task description
        return {
          status: HttpStatus.OK,
          message: 'OK'
        }
      }

      if (isDeposit) {
        walletData.balance = walletData.balance + amount * 2;
        walletData.gamesWin++;
      } else {
        walletData.balance -= amount;
        walletData.gamesLoss++;
      }

      // encrypt wallet data
      const data: string = await encryptWallet(walletData, user.privateKey, user.walletKey);

      // save wallet data
      await this.context.walletRepository.update({
        id: wallet.id,
        data
      });

      return {
        status: HttpStatus.OK,
        message: 'OK',
        data: {
          newBalance: walletData.balance
        }
      }
    } catch (error) {
      return {
        status: HttpStatus.BAD_REQUEST,
        message: error.message || error
      }
    }
  }
}