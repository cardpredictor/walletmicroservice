import jwt from 'jwt-then';
import HttpStatus from 'http-status-codes';
import config from 'config';
import { Context } from '../types/Context';
import { Wallet } from '../types/Wallet';
import NodeRSA from 'node-rsa';
import { WalletData } from '../types/WalletData';
import { decryptText, decryptWallet } from '../lib/utils';
import { ResponseObject } from '../types/ResponseObject';

export class VerifyTokenService {
  context: Context;

  constructor(context: Context) {
    this.context = context;
  }

  async execute(token: string): Promise<ResponseObject> {
    try {
      const decoded = await jwt.verify(token, config.get('JWT.secret'));
      // @ts-ignore
      const user = await this.context.userRepository.getUserById(decoded.id);

      if (!user) {
        return {
          status: HttpStatus.NOT_FOUND,
          message: 'User not found'
        };
      }

      const wallet: Wallet = await this.context.walletRepository.getWalletByUserId(user.id);

      // decrypt wallet data
      const walletData: WalletData = await decryptWallet(wallet.data, user.privateKey, user.walletKey);

      // do not send privateKey, walletKey, passwordSalt and password
      delete user.privateKey;
      delete user.walletKey;
      delete user.password;
      delete user.passwordSalt;

      return {
        status: HttpStatus.OK,
        message: "OK",
        data: {
          user,
          wallet: walletData
        }
      }

    } catch (error) {
      return {
        status: HttpStatus.BAD_REQUEST,
        message: error.message || error
      }
    }
  }
}