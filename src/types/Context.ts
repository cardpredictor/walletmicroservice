import { UserRepository } from '../repository/user';
import { WalletRepository } from '../repository/wallet';

export interface Context {
  userRepository: UserRepository,
  walletRepository: WalletRepository
}