export interface User {
  id?: number | undefined,
  username?: string | undefined,
  password?: string | undefined,
  publicKey?: string | undefined,
  privateKey?: string | undefined,
  walletKey?: string | undefined
  passwordSalt?: string | undefined
}